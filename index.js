// Bài 1:
// Ngày hôm qua
document.getElementById("btnNgayHomQua").onclick = function () {
  // Input: ngày tháng năm, lấy dữ liệu là một chuỗi từ input date, phải chuyển thành kiểu dữ liệu riêng lẻ ngày tháng và năm

  var date = new Date($("#iNgay").val());
  ngay = date.getDate();
  thang = date.getMonth() + 1;
  nam = date.getFullYear();

  // Progress:
  // Các trường hợp ngày hôm qua
  // Nếu người dùng nhập vào các ngày 1 của tháng 3 và là năm nhuận thì ngày hôm qua là ngày 29 của tháng 2, nếu không phải năm nhuận thì là ngày 28/2.
  switch (thang) {
    case 1:
    case 2:
    case 4:
    case 6:
    case 8:
    case 9:
    case 11:
      // Nếu người dùng nhập vào các ngày 1 các tháng 1 2 4 6 8 9 11 thì ngày hôm qua là ngày 31 của tháng trước
      if (ngay == 1) {
        // Nếu người dùng nhập vào ngày 1 tháng 1 thì tháng sẽ là tháng 12 của năm trước
        if (thang == 1) {
          ngay = 31;
          thang = 12;
          nam--;
        } else {
          ngay = 31;
          thang--;
        }
        // Còn lại nếu người dùng nhập vào số ngày trên 1 thì ngày sẽ bị giảm đi 1 đơn vị
      } else if (ngay > 1) {
        ngay--;
      }
      break;
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      // Nếu người dùng nhập vào các ngày 1 các tháng 5 7 8 10 12 thì ngày hôm qua là ngày 30 của tháng trước
      if (ngay == 1) {
        ngay = 30;
        thang--;
        // Còn lại nếu người dùng nhập vào số ngày trên 1 thì ngày sẽ bị giảm đi 1 đơn vị
      } else if (ngay > 1) {
        ngay--;
      }
      break;
    case 3:
      // Nếu người dùng nhập vào các ngày 1 của tháng 3 và là năm nhuận thì ngày hôm qua là ngày 29 của tháng 2, nếu không phải năm nhuận thì là ngày 28/2.
      if ((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0) {
        if (ngay == 1 && thang == 3) {
          ngay = 29;
          thang = 2;
        } else if (ngay > 1) {
          ngay--;
        }
      } else {
        ngay = 28;
        thang = 2;
      }
      break;
    default:
      ngay--;
  }
  //   In kết quả ra giao diện
  document.getElementById("ngayThang").innerHTML =
    "Kết quả: " + ngay + "/" + thang + "/" + nam;
};

// Ngày mai
document.getElementById("btnNgayMai").onclick = function () {
  // Input: ngày tháng năm, lấy dữ liệu là một chuỗi từ input date, phải chuyển thành kiểu dữ liệu riêng lẻ ngày tháng và năm

  var date = new Date($("#iNgay").val());
  ngay = date.getDate();
  thang = date.getMonth() + 1;
  nam = date.getFullYear();

  //   Progress:
  // Các trường hợp có thể xảy ra
  switch (thang) {
    // Nếu ngày nhập vào là ngày 31 của các tháng 1 3 5 7 8 10 12 thì ngày mai là ngày 1 của tháng kế tiếp
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      if (ngay == 31) {
        // Nếu người dùng nhập vào ngày 31 tháng 12 thì ngày mai sẽ là ngày 1 tháng 1 của năm sau
        if (thang == 12) {
          ngay = 1;
          thang = 1;
          nam++;
        } else {
          ngay = 1;
          thang++;
        }
      } else if (ngay < 31) {
        // Nếu ngày nhỏ hơn 31 thì ngày + 1
        ngay++;
      }
      break;
    //   Nếu ngày nhập vào là ngày 30 của các tháng 4 6 9 11 thì ngày mai là ngàu 1 của tháng kế tiếp
    case 4:
    case 6:
    case 9:
    case 11:
      if (ngay == 30) {
        ngay = 1;
        thang++;
      } else {
        ngay++;
      }
      break;
    //   Nếu tháng = 2 và năm nhuận thì ngày mai của ngày 29/2 và ngày 1/3
    case 2:
      if ((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0) {
        if (ngay == 29 && thang == 2) {
          ngay = 1;
          thang = 3;
        }
      } else {
        if (ngay == 28) {
          ngay = 1;
          thang = 3;
        }
      }
      break;
    default:
      ngay++;
  }
  //   In kết quả ra giao diện
  document.getElementById("ngayThang").innerHTML =
    "Kết quả: " + ngay + "/" + thang + "/" + nam;
};

// Bài 2:
document.getElementById("btnTinhNgay").onclick = function () {
  // Input: người dùng chọn tháng 1 đến 12
  var thang = Number(document.getElementById("iNhapThang").value);
  var nam = Number(document.getElementById("iNhapNam").value);

  //   Output: number
  var ngay = 0;

  // năm cần lớn hơn 1920 nếu không chương trình sẽ không được thực hiện
  if (nam < 1921) {
    alert("Năm cần lớn hơn 1920");
    document.getElementById("soNgay").innerHTML = "Dữ liệu không hợp lệ";
  } else {
    // Progress:
    // Những trường hợp có thể xảy ra
    switch (thang) {
      // Đối với trường hợp tháng 1 3 5 7 8 10 12 sẽ có 31 ngày
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        ngay = 31;
        break;
      // Đối với trường hợp tháng 4 6 9 11 sẽ có 30 ngày
      case 4:
      case 6:
      case 9:
      case 11:
        ngay = 30;
        break;
      case 2:
        // Tháng 2 nếu năm nhuận sẽ có 29 ngày, ngược lại sẽ có 28 ngày
        if ((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0) {
          ngay = 29;
        } else {
          ngay = 28;
        }
    }
    //   In kết quả ra giao diện
    document.getElementById("soNgay").innerHTML =
      "Kết quả: tháng " + thang + " năm " + nam + " có " + ngay + " ngày";
  }
};

// Bài 3:
document.getElementById("btnDocSo").onclick = function () {
  // Input: số có 3 chữ số
  var so3ChuSo = Number(document.getElementById("so3ChuSo").value);

  // Nếu người dùng nhập đúng 3 chữ số thì chương trình mới được thực hiện, nếu nhập sai thì sẽ không chạy
  //   Nếu người dùng nhập vào số dưới 99 hoặc trên 990 thì chương trình sẽ không được thực hiện
  if (so3ChuSo < 100 || so3ChuSo > 999) {
    alert("Dữ liệu không hợp lệ");
    document.getElementById("soDoc").innerHTML = "Dữ liệu không hợp lệ";
  } else {
    // Nếu người dùng nhập đúng thì chương trình sẽ chạy tiếp
    // Tách số 3 ký số
    var soHangTram = 0;
    var soHangChuc = 0;
    var soHangDonVi = 0;

    var soHangTram = Math.floor(so3ChuSo / 100);
    var soHangChuc = Math.floor((so3ChuSo / 10) % 10);
    var soHangDonVi = Math.floor((so3ChuSo % 100) % 10);

    // Hiển thị cách đọc ký tự số thành chữ hàng đơn vị
    switch (soHangDonVi) {
      case 1:
        soHangDonVi = "một";
        break;
      case 2:
        soHangDonVi = "hai";
        break;
      case 3:
        soHangDonVi = "ba";
        break;
      case 4:
        soHangDonVi = "bốn";
        break;
      case 5:
        soHangDonVi = "năm";
        break;
      case 6:
        soHangDonVi = "sáu";
        break;
      case 7:
        soHangDonVi = "bảy";
        break;
      case 8:
        soHangDonVi = "tám";
        break;
      case 9:
        soHangDonVi = "chín";
        break;
    }

    // Hiển thị cách đọc ký tự số thành chữ hàng chục
    switch (soHangChuc) {
      case 1:
        soHangChuc = "mười";
        break;
      case 2:
        soHangChuc = "hai mươi";
        break;
      case 3:
        soHangChuc = "ba mươi";
        break;
      case 4:
        soHangChuc = "bốn mươi";
        break;
      case 5:
        soHangChuc = "năm mươi";
        break;
      case 6:
        soHangChuc = "sáu mươi";
        break;
      case 7:
        soHangChuc = "bảy mươi";
        break;
      case 8:
        soHangChuc = "tám mươi";
        break;
      case 9:
        soHangChuc = "chín mươi";
        break;
    }

    // Hiển thị cách đọc ký tự số thành chữ hàng trăm
    switch (soHangTram) {
      case 1:
        soHangTram = "một trăm";
        break;
      case 2:
        soHangTram = "hai trăm";
        break;
      case 3:
        soHangTram = "ba trăm";
        break;
      case 4:
        soHangTram = "bốn trăm";
        break;
      case 5:
        soHangTram = "năm trăm";
        break;
      case 6:
        soHangTram = "sáu trăm";
        break;
      case 7:
        soHangTram = "bảy trăm";
        break;
      case 8:
        soHangTram = "tám trăm";
        break;
      case 9:
        soHangTram = "chín trăm";
        break;
    }

    // Sắp xếp cách đọc cho các trường hợp
    // Output: string
    var ketQuaDocSo = "";

    // Trường hợp số hàng chục và số hàng đơn vị bằng không thì số đó sẽ không được đọc
    if (soHangChuc == 0 && soHangDonVi == 0) {
      ketQuaDocSo = soHangTram;
      // Trường hợp chỉ có số hàng đơn vị bằng không thì số đó sẽ không được đọc
    } else if (soHangDonVi == 0) {
      ketQuaDocSo = soHangTram + " " + soHangChuc;
      // Trường hợp chỉ có số hàng chục bằng không thì sẽ có chữ lẻ ở giữa
    } else if (soHangChuc == 0) {
      ketQuaDocSo = soHangTram + " lẻ " + soHangDonVi;
      // Trường hợp còn lại cả 3 số đều được đọc
    } else {
      ketQuaDocSo = soHangTram + " " + soHangChuc + " " + soHangDonVi;
    }

    // In kết quả ra giao diện
    document.getElementById("soDoc").innerHTML = "Kết quả: " + ketQuaDocSo;
  }
};

// Bài 4:
document.getElementById("btnTimSinhVien").onclick = function () {
  // Input:
  // Tên của các sinh viên và tọa độ của họ
  var tenSinhVien1 = document.getElementById("tenSinhVien1").value;
  var sVx1 = Number(document.getElementById("toaDox1").value);
  var sVy1 = Number(document.getElementById("toaDoy1").value);

  var tenSinhVien2 = document.getElementById("tenSinhVien2").value;
  var sVx2 = Number(document.getElementById("toaDox2").value);
  var sVy2 = Number(document.getElementById("toaDoy2").value);

  var tenSinhVien3 = document.getElementById("tenSinhVien3").value;
  var sVx3 = Number(document.getElementById("toaDox3").value);
  var sVy3 = Number(document.getElementById("toaDoy3").value);

  // Tọa độ của ngôi trường
  var tDoTx = Number(document.getElementById("toaDoTruongX").value);
  var tDoTy = Number(document.getElementById("toaDoTruongY").value);

  // Progress:
  // Tìm số đoạn đường mà mỗi sinh viên đến trường
  var d1 = 0;
  var d2 = 0;
  var d3 = 0;

  d1 = Math.sqrt(Math.pow(tDoTx - sVx1, 2) + Math.pow(tDoTy - sVy1, 2));
  d2 = Math.sqrt(Math.pow(tDoTx - sVx2, 2) + Math.pow(tDoTy - sVy2, 2));
  d3 = Math.sqrt(Math.pow(tDoTx - sVx3, 2) + Math.pow(tDoTy - sVy3, 2));

  // Tạo lệnh so sánh để cho ra kết quả
  var ketQuaTimDuong = "";

  if (d1 > d2 && d1 > d3){
    ketQuaTimDuong = tenSinhVien1;
  }else if (d2 > d1 && d2 > d3){
    ketQuaTimDuong = tenSinhVien2;
  }else if (d3 > d1 && d3 > d2){
    ketQuaTimDuong = tenSinhVien3;
  }

  // In kết quả ra giao diện.
  document.getElementById('sinhVienXaNhat').innerHTML = "Kết quả: sinh viên xa trường nhất là " + ketQuaTimDuong; 
  
};
